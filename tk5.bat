C:
@ECHO OFF
rem ++++++++++++++++++++++++++++++++++++++++++++++++
rem +              copyright 2003 Bob Parnass
rem +
rem + tk5.bat batch file
rem +
rem + Run this file to start the tk5 program
rem ++++++++++++++++++++++++++++++++++++++++++++++++

SET tk5=C:\tk5
"C:\tcl\bin\wish.exe"  "C:\tk5\tk5.tcl"
